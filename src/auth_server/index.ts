import express from 'express';
import bodyParser from 'body-parser';
import { authRequestController } from './controller';
import { jwtAuthorizer } from '../core/jwt_utils';
import { core } from '../core';


const app = express();
const PORT = 4000;

core.init();
const jsonParser = bodyParser.json();
app.use(jsonParser);

/**
 * Auth routes:
 * /auth/register
 * /auth/login
 * /auth/logout
 * /auth/refresh
 */

app.post('/auth/register', authRequestController.loginUser('register'));

app.post('/auth/login', authRequestController.loginUser('login'));

app.get('/auth/logout', jwtAuthorizer.validateToken('access'), authRequestController.logoutUser);

app.get('/auth/refresh', jwtAuthorizer.validateToken('refresh'), authRequestController.refreshToken);

app.listen(PORT, () => console.log(`Server is running on ${PORT} port!`));