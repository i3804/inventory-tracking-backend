import { RequestHandler } from "express";
import { RequestController } from "../../core/controller";
import { jwtAuthorizer } from "../../core/jwt_utils";
import { authDBController } from "../db";
import { DeleteRefreshOperation, LoggingOperation, RefreshTokenOperation } from "../db/types";


class AuthRequestController extends RequestController {

    private handlerProxyModified(callback: (req) => Promise<[Promise<any>, number]>): RequestHandler {
        const callbackHandler = (res) => async (promiseArg: [promise: Promise<any>, code: number]): Promise<void> => {
            const promise = promiseArg[0];
            const code = promiseArg[1];

            let result = await promise;

            if (result) {
                res.status(code).json(result);
            } else {
                res.sendStatus(code);
            }
        }

        return this.handlerProxy(callback, callbackHandler);
    }

    loginUser = (type: 'login' | 'register'): RequestHandler => this.handlerProxyModified(async (req) => {
        try {
            const data = this.mapThroughObjectKeys<LoggingOperation>(req.body, ['login', 'password']);
            const method = type === 'login' ? authDBController.login.bind(authDBController) : authDBController.register.bind(authDBController);
            const userInfo = await method(data);
    
            const accessToken = jwtAuthorizer.getAccessToken({login: data.login, roles: userInfo});
            const refreshToken = jwtAuthorizer.getRefreshToken({login: data.login, roles: userInfo});

            await authDBController.saveRefresh(refreshToken, data.login);
            
            return [Promise.resolve({accessToken, refreshToken}), 201];
        } catch (e) {
            if (e instanceof Promise) {
                return e;
            }

            return Promise.reject([401, e]);
        }
    });

    logoutUser: RequestHandler = this.handlerProxyModified(async (req) => {
        const data = this.mapThroughObjectKeys<DeleteRefreshOperation>(req.body, ['login']);
        return [authDBController.deleteRefresh(data.login), 204];
    });

    refreshToken: RequestHandler = this.handlerProxyModified(async (req) => {
        const data = this.mapThroughObjectKeys<RefreshTokenOperation>(req.body, ['login', 'roles']);
        const token = req.header('authorization').toString().split(' ')[1];

        try {
            await authDBController.checkRefresh(token);

            const accessToken = jwtAuthorizer.getAccessToken(data);
            return [Promise.resolve({accessToken}), 201];
        } catch (e) {
            if (e instanceof Promise) {
                return e;
            }

            return Promise.reject([500, e]);
        }
    });
}

const authRequestController = new AuthRequestController();
export {authRequestController};