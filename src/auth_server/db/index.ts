import { DBController } from "../../core/db";
import { LoggingOperation, User } from "./types";
import { SHA256 } from "crypto-js";
import { core } from "../../core";


export class AuthDBContoller extends DBController {
    constructor() {
        super('AuthDBContoller');
    }

    async login(payload: LoggingOperation): Promise<string[]> {
        try {
            const {login, password} = payload;

            const [row, _] = await this.connection.execute('SELECT * FROM `users` WHERE login = ? LIMIT 1;', [login]);
            if (Array.isArray(row)) {
                const userObject = row[0] as User;

                if (userObject.password === SHA256(password).toString()) {
                    const promises: Promise<string>[] = userObject.roles.map(async role => {
                        const selectedRole = await core.roleDBController.selectRole(role);
                        return selectedRole.name;
                    });

                    return Promise.all(promises).then(roles => {
                        console.log('user roles', roles);
                        return roles;
                    }).catch((e) => {
                        console.log(e);
                        return Promise.reject(["Unable to fetch user role", 500]);
                    });
                } else {
                    return Promise.reject(["Invalid credentials!", 401]);
                }
            }
        } catch (e) {
            return Promise.reject(["Invalid credentials!", 401])
        }
    }

    async register(payload: LoggingOperation): Promise<string[]> {
        const {login, password} = payload;

        const [row, _] = await this.connection.execute('SELECT * FROM `users` WHERE login = ?;', [login]);
        if (Array.isArray(row)) {
            if (row.length !== 0) {
                return Promise.reject(["Provided login already exists!", 401]);
            }
        }
        
        const baseRole = await core.roleDBController.getBaseRole();
        const hashedPassword = SHA256(password).toString();

        try {
            await this.connection.execute('INSERT INTO `users`(login, password, roles) VALUES(?, ?, ?);', [login, hashedPassword, JSON.stringify([baseRole.id])]);
            return [baseRole.name];
        } catch (e) {
            console.log('error', e);
            return Promise.reject(["Smth went wrong while registering a user"]);
        }
    }

    async saveRefresh(token: string, login: string): Promise<void> {
        try {
            await this.connection.execute('INSERT INTO `valid_refresh_tokens`(value, login) VALUES(?, ?);', [token, login]);
        } catch (e) {
            return Promise.reject(['Error occurred while executing insert query!', 500]);
        }
    }

    async checkRefresh(token: string): Promise<void> {
        try {
            const [row, _] = await this.connection.execute('SELECT * FROM `valid_refresh_tokens` WHERE value = ? LIMIT 1;', [token]);

            if (Array.isArray(row)) {
                if (row.length > 0) {
                    return Promise.resolve();
                }
                return Promise.reject('This refresh token isn\'t valid!');
            }
        } catch (e) {
            return Promise.reject(['Error while executing select query!', 500]);
        }
    }

    async deleteRefresh(login: string): Promise<void> {
        try {
            console.log('login is', login);
            await this.connection.execute('DELETE FROM `valid_refresh_tokens` WHERE login = ? LIMIT 1;', [login]);
        } catch (e) {
            return Promise.reject(['Error occurred while executing delete query!', 500]);
        }
    }
}

const authDBController = new AuthDBContoller();

export {authDBController};