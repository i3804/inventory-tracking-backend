import { Role } from "../../core/roles";

export interface LoggingOperation {
    login: string;
    password: string;
}

export interface RegisterOperation extends LoggingOperation {
    roles: Role[];
}

export interface RefreshTokenOperation {
    login: string;
    roles: string[];
}

export interface User {
    id: number;
    login: string;
    password: string;
    roles: Array<number>;
}

export interface DeleteRefreshOperation {
    login: string;
}
