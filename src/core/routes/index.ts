import { DBController } from "../db";

type Route = {id: number, path: string};

export class RoutesDBController extends DBController {

    static DB_CONNECT_EVENT_NAME = "ROUTE_CONTROLLER";
    
    routes: string[] = [];

    constructor() {
        super(RoutesDBController.DB_CONNECT_EVENT_NAME);
    }

    async loadRoutes() {
        const [rows, _] = await this.connection.execute('SELECT * FROM `routes`;');
        
        if (Array.isArray(rows)) {
            try {
                this.routes = (rows as Array<Route>).map(row => row.path);
            } catch (e) {
                console.log('An error occurred while trying to load routes!', e);
            }
        }
    }
}
