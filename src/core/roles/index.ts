import { DBController } from "../db";
import { flattenDeep } from "lodash";
import { RoutesDBController } from "../routes";


export type Route = {type: "string" | "regexp", value: string}
export type Role = {id: number, name: string, extends?: number, routes: Array<Route>}

export class RoleDBController extends DBController {
    roles: Map<number, Array<string>> = new Map();
    rolesNames: Map<number, string> = new Map();
    static DB_CONNECT_EVENT_NAME = "ROLE_CONTROLLER";
    private routesController: RoutesDBController;

    constructor(routesController: RoutesDBController) {
        super(RoleDBController.DB_CONNECT_EVENT_NAME);
        this.routesController = routesController;
    }

    async selectRole(id?: string | number): Promise<Role> {
        let chooseBy = id || 1;
        let searchResult: any = null;

        if (typeof chooseBy === 'number') {
            const [row, _] = await this.connection.execute('SELECT * FROM `roles` WHERE id = ?', [chooseBy]);
            searchResult = row;
        } else {
            const [row, _] = await this.connection.execute('SELECT * FROM `roles` WHERE name = ?', [chooseBy]);
            searchResult = row;
        }

        if (Array.isArray(searchResult)) {
            if (searchResult.length > 0) {
                const routes = searchResult[0]['routes'] as Array<Route>
    
                const roleObject: Role = {
                    id: searchResult[0]['id'],
                    name: searchResult[0]['name'],
                    extends: searchResult[0]['extends'],
                    routes
                }
                return roleObject;
            }
        }

    }

    async getBaseRole(): Promise<{id: number, name: string}> {
        const baseRole = await this.selectRole('base');
        return {id: baseRole.id, name: baseRole.name};
    }

    async loadRoles() {
        try {
            const [baseRows, _] = await this.connection.execute('SELECT * FROM `roles` WHERE extends IS NULL;');

            if (Array.isArray(baseRows)) {
                (baseRows as Array<Role>).forEach(role => {
                    const routes = flattenDeep(role.routes.map(routeObj => this.parseRoleRoute(routeObj)));
                    this.roles.set(role.id, routes);
                    this.rolesNames.set(role.id, role.name);
                });
            }

            const [ingeritedRows, __] = await this.connection.execute('SELECT * FROM `roles` WHERE extends IS NOT NULL;');
            if (Array.isArray(ingeritedRows)) {
                (ingeritedRows as Array<Role>).forEach(role => {
                    const routes = flattenDeep(role.routes.map(routeObj => this.parseRoleRoute(routeObj)));
                    const inheritedRoutes = this.roles.get(role.extends);

                    const mergedRolesArray = Array.from(new Set([...routes, ...inheritedRoutes]));
                    this.roles.set(role.id, mergedRolesArray);
                });
            }

        } catch (e) {
            console.log("Error occurred while trying to load roles!", e);
        }
    }

    checkRouteAvailability(roleName: string, route: string): boolean {
        let roleId = -1; 
        this.rolesNames.forEach((value, key) => {
            if (value === roleName) {
                roleId = key;
            }
        });

        if (roleId >= 0) {
            return this.roles.get(roleId).includes(route);
        }

        return false;
    }

    private parseRoleRoute(routeObj: {type: "string" | "regexp", value: string}): string | string[] {
        if (routeObj.type !== "string") {
            const routeRegex = new RegExp(routeObj.value);
            const matchedRoutes: string[] = [];
            
            this.routesController.routes.forEach(route => {
                if (route.search(routeRegex) === 0) {
                    matchedRoutes.push(route);
                } 
            })
            
            return matchedRoutes;
        } 
        return routeObj.value;
    }
}