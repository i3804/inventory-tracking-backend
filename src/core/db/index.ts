import mysql from 'mysql2/promise';
import bluebird from 'bluebird';
import path from "path";
import os from 'os';
import fs from 'fs';
import EventEmitter from 'events';

export const dbConnectionEventEmitter = new EventEmitter();

export class DBController {
    protected connection: mysql.Connection;
    protected DB_CONFIG_PATH = path.resolve(os.homedir(), '.config', 'inventory_tracking', 'mysql', 'db.config.json');
    static DB_CONNECT_EVENT_NAME = 'DEFAULT_CONTROLLER';

    constructor(eventName = DBController.DB_CONNECT_EVENT_NAME) {
        let dbConfigJson: mysql.ConnectionOptions;
        
        try {
            const dbConfigRaw = fs.readFileSync(this.DB_CONFIG_PATH);
            dbConfigJson = (JSON.parse(dbConfigRaw.toString())) as mysql.ConnectionOptions;
            dbConfigJson.Promise = bluebird;

            mysql.createConnection(dbConfigJson).then((connection) => {
                this.connection = connection;
                console.log(`Connection to database was successfully established! ${eventName}`);
                dbConnectionEventEmitter.emit(eventName);
            }).catch((error) => {
                console.log('Error occurred while establishing connection with database', error);
            });
        } catch (e) {
            console.log('Some error occurred!', e);
        }
    }
}