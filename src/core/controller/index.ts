import { RequestHandler } from "express";
import { isEqual } from "lodash";


export class RequestController {
    protected handlerProxy = 
        (callback: (req) => Promise<[Promise<any>, number]>, 
        callbackHadler: (res) => (promiseArg: [promise: Promise<any>, code: number], ...args) => Promise<void>): RequestHandler => {

            return (req, res) => {
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Origin, Authorization");
                res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
                res.header("Content-Security-Policy", "default-src *; connect-src *; script-src *; object-src *;");
                res.header("X-Content-Security-Policy", "default-src *; connect-src *; script-src *; object-src *;");
                res.header("X-Webkit-CSP", "default-src *; connect-src *; script-src 'unsafe-inline' 'unsafe-eval' *; object-src *;");

                callback(req)
                    .then(callbackHadler(res))
                    .catch(([errorCode, e]) => {
                        console.log('error is', errorCode, e);
                        res.status(errorCode).send(e);
                    });
            }
    }

    protected mapThroughObjectKeys = <T>(obj: {[key: string]: any}, validKeys: string[]): T => {
        for (const key in obj) {
            if (!validKeys.includes(key)) {
                delete obj[key];
            }
        }

        if (!isEqual(Object.keys(obj).sort(), validKeys.sort())) {
            console.warn(`object ${obj} has missing keys!`);
        }

        return obj as T;
    }
}