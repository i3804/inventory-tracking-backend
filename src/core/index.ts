import { dbConnectionEventEmitter } from "./db";
import { RoleDBController } from "./roles";
import { RoutesDBController } from "./routes";


class Core {
    routesDBController: RoutesDBController;
    roleDBController: RoleDBController;

    init() {
        this.routesDBController = new RoutesDBController();
        
        dbConnectionEventEmitter.on(RoutesDBController.DB_CONNECT_EVENT_NAME, () => {
            this.routesDBController.loadRoutes().then(() => {
                this.roleDBController = new RoleDBController(this.routesDBController);
            });
        });
        
        dbConnectionEventEmitter.on(RoleDBController.DB_CONNECT_EVENT_NAME, () => {
            this.roleDBController.loadRoles();
        });
    }
}

const core = new Core();
export {core};