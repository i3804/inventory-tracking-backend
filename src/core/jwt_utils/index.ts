import path from "path";
import os from 'os';
import fs from 'fs';
import { RequestHandler } from "express";
import jwt, { JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken';

type JWTTokenInfo = {login: string, roles: string[]};

class JWTAuthorizer {

    private JWT_CONFIG_PATH = path.resolve(os.homedir(), '.config', 'inventory_tracking', 'jwt.config.json');
    private ACCESS_SECRET: string;
    private REFRESH_SECRET: string;

    constructor() {
        try {
            const jwtConfig: {accessSecret: string, refreshSecret: string} = JSON.parse(fs.readFileSync(this.JWT_CONFIG_PATH).toString());
            this.ACCESS_SECRET = jwtConfig.accessSecret;
            this.REFRESH_SECRET = jwtConfig.refreshSecret;
        } catch (e) {
            console.log('Unable to load jwt config!');
        }
    }

    validateToken = (type: 'access' | 'refresh'): RequestHandler => (req, res, next) => {
        if (!req.header('authorization')) {
            return res.sendStatus(401);
        }
        
        const token = req.header('authorization').toString().split(' ')[1];

        jwt.verify(token, type === 'access' ? this.ACCESS_SECRET : this.REFRESH_SECRET, (error, info: JWTTokenInfo) => {
            if (error instanceof TokenExpiredError) {
                return res.status(403).send("Token expired, make sure to refresh!");
            } 
            if (error instanceof JsonWebTokenError) {
                return res.status(406).send("Invalid token!");
            }

            req.body = {
                ...req.body, 
                ...info
            }
            next();
        });
    }

    getAccessToken(info: JWTTokenInfo): string {
        return jwt.sign(info, this.ACCESS_SECRET, {expiresIn: '15m'});
    }

    getRefreshToken(info: JWTTokenInfo): string {
        return jwt.sign(info, this.REFRESH_SECRET, {expiresIn: '365d'});
    }
}

const jwtAuthorizer = new JWTAuthorizer();
export {jwtAuthorizer};