import WebSocket, { ServerOptions, WebSocketServer } from 'ws';
import { SocketEventType } from './types';

interface SocketEventPayload {
    socketEventType: SocketEventType, 
    data: Record<string, any>;
}

class CustomSocketServer extends WebSocketServer {
    static SOCKET_EVENT = 'data_changed';
    static WEBSOCKET_SERVER_PORT = 8080;

    constructor(options?: ServerOptions, callback?: () => void) {
        super(options, callback);
        console.log(`Socket server has started on ${CustomSocketServer.WEBSOCKET_SERVER_PORT} port!`);
    }

    emitSocketEvent(payload: SocketEventPayload) {
        this.emit(CustomSocketServer.SOCKET_EVENT, payload);
    }

    onSocketEvent(payload: SocketEventPayload) {
        console.log('event type is', payload.socketEventType);
        console.log('event payload is', payload.data);
        this.clients.forEach((client) => {
            if (client.readyState === WebSocket.OPEN) {
                client.send(JSON.stringify(payload));
            }
        });
    }

    initialize() {
        this.on(CustomSocketServer.SOCKET_EVENT, this.onSocketEvent);
    }
}

const socketServer = new CustomSocketServer({
    clientTracking: true,
    port: CustomSocketServer.WEBSOCKET_SERVER_PORT
});
 
export {socketServer};