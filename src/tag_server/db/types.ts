export interface DevicesTableScheme {
    id: number;
    device_type: number;
    created_at: string;
    absence: number;
    active: number;
    took_at: string;
    deleted_at: string;
    hash: string;
}

export interface DevicesTypesScheme {
    id: number;
    name: string;
    picture: string;
}