import { DevicesTableScheme, DevicesTypesScheme } from './types';
import { ActivatingOrDeactivatingTag, AddingDeviceType, AddingOrUpdatingTag, DeletingDeviceType, TogglingOrDeletingTag } from '../contoller/types';
import moment from 'moment';
import { DBController } from '../../core/db';
import { ResultSetHeader } from 'mysql2';


export class TagsDBController extends DBController {

    async addDevice(data: AddingOrUpdatingTag): Promise<Partial<DevicesTableScheme>> {
        try {
            await this.connection.execute(
                'INSERT INTO `devices`(hash, device_type, created_at) VALUES (?, ?, ?)', 
                [data.hash, data.device_type, moment().format('YYYY-MM-DD HH:mm:ss')]
            );

            return this.getTag(data.hash).then((tag) => {
                return tag;
            }).catch((e) => {
                return Promise.reject(e);
            })
        } catch(e) {
            return Promise.reject(['Error occurred while executing insert query!', 500]);
        }
    }

    async deleteDevice(data: TogglingOrDeletingTag): Promise<{hash: string}> {
        try {
            // fix to update query
            await this.connection.execute(
                'UPDATE `devices` SET deleted_at = ? WHERE hash = ? LIMIT 1;', 
                [moment().format('YYYY-MM-DD HH:mm:ss'), data.hash]
            );
            return {hash: data.hash};
        } catch (e) {
            return Promise.reject(['Error occurred while executing delete query!', 500]);
        }
    }

    async updateDevice(data: AddingOrUpdatingTag): Promise<Partial<DevicesTableScheme>> {
        try {
            await this.connection.execute('UPDATE `devices` SET device_type = ? WHERE hash = ? LIMIT 1;', [data.device_type, data.hash]);
            return this.getTag(data.hash).then((tag) => {
                return tag;
            }).catch((e) => {
                return Promise.reject(e);
            })
        } catch (e) {
            return Promise.reject(['Error occurred while executing update query!', 500]);
        }
    }

    // async toggleDevice(data: TogglingOrDeletingTag): Promise<Partial<DevicesTableScheme>> {
    //     return this.connection.execute('SELECT absence FROM `devices` WHERE hash = ? LIMIT 1;', [data.hash])
    //         .then(async ([rows, _]) => {
    //             if (Array.isArray(rows)) {
    //                 if (rows.length) {
    //                     let newAbsenceState = Math.abs((rows[0] as Partial<DevicesTableScheme>).absence - 1);
                        
    //                     try {
    //                         if (newAbsenceState === 1) {
    //                             await this.connection.execute(
    //                                 'UPDATE `devices` SET absence = ?, took_at = ? WHERE hash = ? LIMIT 1;', 
    //                                 [newAbsenceState, moment().format('YYYY-MM-DD HH:mm:ss'), data.hash]
    //                             );
    //                         } else {
    //                             await this.connection.execute('UPDATE `devices` SET absence = ? WHERE hash = ? LIMIT 1;', [newAbsenceState, data.hash]);
    //                         }

    //                         return this.getTag(data.hash).then((tag) => {
    //                             return tag;
    //                         }).catch((e) => {
    //                             return Promise.reject(e);
    //                         });
    //                     } catch (e) {
    //                         return Promise.reject(['Error occurred while executing update query!', 500]);
    //                     }
    //                 }
    //             }
    //         }).catch((e) => {
    //             return Promise.reject(['Error occurred while executing select query!', 500]);
    //         });
    // }
    
    async toggleDevice(data: TogglingOrDeletingTag): Promise<Partial<DevicesTableScheme>> {;
        try {
            if (data.absense) {
                await this.connection.execute('UPDATE `devices` SET absence = ?, took_at = ? WHERE hash = ? LIMIT 1;', [1, moment().format('YYYY-MM-DD HH:mm:ss'), data.hash]);
            } else {
                await this.connection.execute('UPDATE `devices` SET absence = ? WHERE hash = ? LIMIT 1;', [0, data.hash]);
            }

            return this.getTag(data.hash).then((tag) => {
                return tag;
            }).catch((e) => {
                return Promise.reject(e);
            });
        } catch (e) {
            console.log('toggle device error', e);
            return Promise.reject(['Error occurred while executing update query!', 500]);
        }
    } 

    async changeActiveState(data: ActivatingOrDeactivatingTag): Promise<Partial<DevicesTableScheme>> {
        try {
            await this.connection.execute('UPDATE `devices` SET active = ? WHERE hash = ? LIMIT 1;', [data.active ? 1 : 0, data.hash]);
            return this.getTag(data.hash).then((tag) => {
                return tag;
            }).catch((e) => {
                return Promise.reject(e);
            });
        } catch (e) {
            return Promise.reject(['Error occurred while executing update query!', 500])
        }
    }

    private async getTag(hash: string): Promise<Partial<DevicesTableScheme>> {
        try {
            const [row, _] = await this.connection.execute('SELECT * FROM `devices` WHERE hash = ? LIMIT 1;', [hash]);
            if (Array.isArray(row)) {
                return row[0] as Partial<DevicesTableScheme>;
            }
        } catch (e) {
            return Promise.reject(['Error occurred while executing select query!', 500]);
        }
    }

    async getAllTags(): Promise<Array<Partial<DevicesTableScheme>>> {
        try {
            const [rows, _] = await this.connection.execute('SELECT * FROM `devices`;');
            if (Array.isArray(rows)) { 
                return rows as Array<Partial<DevicesTableScheme>>;
            }
            return [];
        } catch (e) {
            return Promise.reject([500, 'Error occurred while executing select query!']);
        }
    }

    async getAllDevicesTypes(): Promise<Array<DevicesTypesScheme>> {   
        try {
            const [rows, _] = await this.connection.execute('SELECT * FROM `device_types`;')
            if (Array.isArray(rows)) {
                return rows as Array<DevicesTypesScheme>;
            }
            return [];
        } catch (e) {
            return Promise.reject(['Error occurred while executing select query!', 500]);
        }
    }

    async addDeviceType(data: AddingDeviceType): Promise<DevicesTypesScheme> {
        try {
            let [row, _] = await this.connection.execute('INSERT INTO `device_types`(name) VALUES(?)', [data.name]);
            row = (row as ResultSetHeader);

            return this.getDeviceType(row.insertId).then(deviceType => {
                return deviceType
            }).catch(e => {
                return Promise.reject(e);
            });
        } catch (e) {
            return Promise.reject(['Error occurred while executing insert query!', 500]);
        }
    }

    async deleteDeviceType(data: DeletingDeviceType): Promise<{id: number}> {
        try {
            await this.connection.execute('DELETE FROM `device_types` WHERE id = ? LIMIT 1;', [data.id]);
            return {id: data.id};
        } catch (e) {
            return Promise.reject(['Error occurred while executing delete query!', 500]);
        }
    }

    private async getDeviceType(id: number): Promise<DevicesTypesScheme> {
        try {
            const [row, _] = await this.connection.execute('SELECT * FROM `device_types` WHERE id = ?', [id]);
            if (Array.isArray(row)) {
                return row[0] as DevicesTypesScheme;
            }
        } catch (e) {
            return Promise.reject(['Error occurred while executing select query!', 500]);
        }
    }
}

const tagsDBController = new TagsDBController();

export {tagsDBController};