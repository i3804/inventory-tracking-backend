export interface TogglingOrDeletingTag {
    hash: string;
    absense: boolean;
}

export interface AddingOrUpdatingTag {
    hash: string;
    device_type: number;
}

export interface ActivatingOrDeactivatingTag {
    hash: string;
    active: boolean
}

export interface AddingDeviceType {
    name: string;
}

export interface DeletingDeviceType {
    id: number;
}