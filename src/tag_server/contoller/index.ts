import { RequestHandler } from "express";
import {tagsDBController} from '../db';
import sha256 from 'crypto-js/sha256';
import { ActivatingOrDeactivatingTag, AddingDeviceType, AddingOrUpdatingTag, DeletingDeviceType, TogglingOrDeletingTag } from "./types";
import moment from "moment";
import { SocketEventType } from "../client_websocket/types";
import { socketServer } from "../client_websocket";
import { RequestController } from "../../core/controller";


class TagsRequestController extends RequestController {
    private handlerProxyModified(callback: (req) => Promise<[Promise<any>, number]>, socketEventType?: SocketEventType): RequestHandler {
        const callbackHandler = (res) => async (promiseArg: [promise: Promise<any>, code: number]): Promise<void> => {
           const promise = promiseArg[0];
           const code = promiseArg[1];

           let result = await promise;

           if (result && !socketEventType) {
            if (typeof result === 'string') {
                res.status(code).send(result);
            } else {
                res.status(code).json(result);
            }
            } else {
                res.sendStatus(code);
            }

            if (socketEventType) {
                socketServer.emitSocketEvent({
                    socketEventType,
                    data: result
                });
            }
        }

        return this.handlerProxy(callback, callbackHandler);
    }

    fetchingTags: RequestHandler = this.handlerProxyModified(async () => {
        return [tagsDBController.getAllTags(), 200];   
    }); 

    generateHash: RequestHandler = this.handlerProxyModified(async () => {
        const newHash = sha256(moment().toString());
        if (newHash.toString().length > 0) {
            return [Promise.resolve({hash: newHash.toString()}), 200];
        } else {
            return Promise.reject([500, "Unable to generate new hash!"]);
        }
    });

    addingTag: RequestHandler = this.handlerProxyModified(async (req) => {
        const data = this.mapThroughObjectKeys<AddingOrUpdatingTag>(req.body, ['hash', 'device_type']);
        return [tagsDBController.addDevice(data), 201];
    }, SocketEventType.DEVICE_ADDED);

    deletingTag: RequestHandler = this.handlerProxyModified(async (req) => {
        const data = this.mapThroughObjectKeys<TogglingOrDeletingTag>(req.body, ['hash', 'absense']);
        return [tagsDBController.deleteDevice(data), 200];
    }, SocketEventType.DEVICE_DELETED);

    updatingTag: RequestHandler = this.handlerProxyModified(async (req) => {
        const data = this.mapThroughObjectKeys<AddingOrUpdatingTag>(req.body, ['hash', 'device_type']);
        return [tagsDBController.updateDevice(data), 200];
    }, SocketEventType.DEVICE_UPDATED);

    changeTrackingStatusOfTag(type: 'active' | 'inactive'): RequestHandler {
        return this.handlerProxyModified(async (req) => {
            const data: ActivatingOrDeactivatingTag = {hash: req.body.hash, active: type === 'active'};
            return [tagsDBController.changeActiveState(data), 200];
        }, SocketEventType.DEVICE_TRACKING_STATE_CHANGED);
    };

    fetchingDevicesTypes: RequestHandler = this.handlerProxyModified(async () => {
        return [tagsDBController.getAllDevicesTypes(), 200];
    });

    addingDeviceType: RequestHandler = this.handlerProxyModified(async (req) => {
        const data = this.mapThroughObjectKeys<AddingDeviceType>(req.body, ['name']);
        return [tagsDBController.addDeviceType(data), 201];
    }, SocketEventType.DEVICE_TYPE_ADDED);

    deletingDeviceType: RequestHandler = this.handlerProxyModified(async (req) => {
        const data = this.mapThroughObjectKeys<DeletingDeviceType>(req.body, ['id']);
        return [tagsDBController.deleteDeviceType(data), 200];
    }, SocketEventType.DEVICE_TYPE_DELETED);
}

const tagsReqController = new TagsRequestController();
export {tagsReqController};