import express from 'express';
import { tagsReqController } from './contoller';
import bodyParser from 'body-parser';
import { mqttServer } from './tags_mqtt';
import {socketServer} from './client_websocket';
import { core } from '../core';
import { jwtAuthorizer } from '../core/jwt_utils';
import { bleMQTTServer } from './ble_mqtt';

mqttServer.start();
bleMQTTServer.start();
socketServer.initialize();

const app = express();
const PORT = 3000;
core.init();

/**
 * All routes:
 * /tag/generate_hash - generate hash for new device
 * /tag/fetch - fetching all tags
 * /tag/add - adding tag to tracking system
 * /tag/delete - deleting tag from tracking system
 * /tag/update - updating tag params
 * /tag/activate - activate tracking of particular tag
 * /tag/deactivate - deactivate tracking of particular tag
 * /device_type/fetch - fetching all devices types
 * /device_type/add - adding device type
 * /device_type/delete - deleting device type
 */
const jsonParser = bodyParser.json();
app.use(jsonParser);
app.use(jwtAuthorizer.validateToken('access'));

// app.get('/tag/generate', tagsReqController.generateHash);

app.get('/tag/fetch', tagsReqController.fetchingTags);

app.post('/tag/add', tagsReqController.addingTag);

app.post('/tag/delete', tagsReqController.deletingTag);

app.post('/tag/update', tagsReqController.updatingTag);

app.post('/tag/activate', tagsReqController.changeTrackingStatusOfTag('active'));

app.post('/tag/deactivate', tagsReqController.changeTrackingStatusOfTag('inactive'));

app.get('/device_type/fetch', tagsReqController.fetchingDevicesTypes);

app.post('/device_type/add', tagsReqController.addingDeviceType);

app.post('/device_type/delete', tagsReqController.deletingDeviceType);

app.listen(PORT, () => console.log(`Server is running on ${PORT} port!`));
