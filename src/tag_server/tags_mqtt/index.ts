import mqtt from 'async-mqtt';
import { socketServer } from '../client_websocket';
import { SocketEventType } from '../client_websocket/types';
import { tagsDBController } from '../db';


class CustomMQTTServer {
    private static BROKER = 'tcp://broker.hivemq.com';
    private static TAG_TOPIC = '/isu/lab/rfid/tag/toggle';
    private mqttClient: mqtt.AsyncClient = null;

    constructor() {
        this.mqttClient = mqtt.connect(CustomMQTTServer.BROKER);

        this.mqttClient.on('connect', async () => {
            this.mqttClient.subscribe(CustomMQTTServer.TAG_TOPIC, {qos: 1}).then(() => {
                console.log(`Connection to topic ${CustomMQTTServer.TAG_TOPIC} via broker ${CustomMQTTServer.BROKER} was established!`);
            }).catch((e) => {
                console.log(`Unable to connect to topic ${CustomMQTTServer.TAG_TOPIC} via broker ${CustomMQTTServer.BROKER}! Falled back with error`, e);
            });
        });
    }

    start() {              
        this.mqttClient.on('message', (topic, message) => {
            const payload = message.toString();
            if (topic === CustomMQTTServer.TAG_TOPIC && payload.length === 64) {
                // tagsDBController.toggleDevice({hash: payload})
                // .then((tag) => {
                //     console.log('Tag is', tag);
                //     socketServer.emitSocketEvent({
                //         socketEventType: SocketEventType.DEVICE_TOGGLED,
                //         data: tag
                //     });
                //     console.log('Device was toggled!');
                // }).catch((e) => {
                //     console.log('Error occurred while toggling device state:', e);
                // });
            }
        });
    }
}

const mqttServer = new CustomMQTTServer();

export {mqttServer};