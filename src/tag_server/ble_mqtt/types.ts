export interface BLETag {
    tag: string;
    distance: number;
}