import mqtt from 'async-mqtt';
import { socketServer } from '../client_websocket';
import { SocketEventType } from '../client_websocket/types';
import { tagsDBController } from '../db';
import { BLETracker } from './controller';
import { BLETag } from './types';


class BLEMQTTServer {
    private static BROKER = 'tcp://broker.hivemq.com';
    private static PRIMARY_TOPIC = '/isu/inventory_tracking/ble/';
    private rooms: string[] = ['402']; // this should be loaded from somewhere
    private mqttClient: mqtt.AsyncClient = null;

    constructor() {
        this.mqttClient = mqtt.connect(BLEMQTTServer.BROKER);

        this.mqttClient.on('connect', async () => {
            this.rooms.forEach(room => {
                const topicToConnect = BLEMQTTServer.PRIMARY_TOPIC + room;

                this.mqttClient.subscribe(BLEMQTTServer.PRIMARY_TOPIC + room, {qos: 1}).then(() => {
                    console.log(`Connection to topic ${topicToConnect} via broker ${BLEMQTTServer.BROKER} was established!`);
                }).catch((e) => {
                    console.log(`Unable to connect to topic ${topicToConnect} via broker ${BLEMQTTServer.BROKER}! Falled back with error`, e);
                });
            });
        });
    }

    start() {
        this.mqttClient.on('message', (topic, message) => {
            try {
                const room = topic.split('/').pop();
                const payload = JSON.parse(message.toString()) as BLETag;

                const result = BLETracker.checkBleTags(payload, room);
                if (result) {
                    console.log('check ble result', result);
                    tagsDBController.toggleDevice({hash: result.tag, absense: result.absense})
                        .then((tag) => {
                            socketServer.emitSocketEvent({
                                socketEventType: SocketEventType.DEVICE_TOGGLED,
                                data: tag
                            });
                        })
                        .catch((e) => {
                            console.log('Error occurred while toggling device state:', e);
                        });
                }
            } catch (e) {
                console.warn(`Oops error occurred, while listening to mqtt topic: ${topic}`, e)
            }
        })
    }
}

const bleMQTTServer = new BLEMQTTServer();
export {bleMQTTServer};