import { BLETag } from "./types";

export class BLETracker {
    private static BLEHashMaps: Map<string, Map<string, number>> = new Map();
    private static presenseCandidates: Map<string, number> = new Map();
    private static limitDistance = 3;
    private static absenseInRow = 5;
    private static presenseInRow = 5;

    static checkBleTags(bleTag: BLETag, room: string): {tag: string, absense: boolean} | void {
        let targetHashMap: Map<string, number> = null;
        console.log('mqtt payload', bleTag);

        if (!BLETracker.BLEHashMaps.get(room)) {
            const newRoomHashMap: Map<string, number> = new Map();
            BLETracker.BLEHashMaps.set(room, newRoomHashMap);
            targetHashMap = newRoomHashMap;
        } else {
            targetHashMap = BLETracker.BLEHashMaps.get(room);
        }

        // console.log('target hash map is', targetHashMap);

        if (targetHashMap.has(bleTag.tag)) {
            let absenseCounter = targetHashMap.get(bleTag.tag);

            if (bleTag.distance <= BLETracker.limitDistance) {
                // обнуляем счетчик отсутствия
                // console.log('tag is on place again!');
                targetHashMap.set(bleTag.tag, 0);
                return;
            } else {
                absenseCounter++;

                if (absenseCounter === BLETracker.absenseInRow) {
                    // удаляем из хэш мэпа и обращение к бд
                    // console.log('tag is missing now!');
                    targetHashMap.delete(bleTag.tag);
                    return {tag: bleTag.tag, absense: true};
                } else {
                    // инкремент счетчика отсутствия
                    // console.log('increment absense counter!');
                    targetHashMap.set(bleTag.tag, absenseCounter);
                    return;
                }
            }
        } else if (BLETracker.presenseCandidates.has(bleTag.tag)) {
            // console.log('presense candidates', BLETracker.presenseCandidates);
            if (bleTag.distance <= BLETracker.limitDistance) {
                let presenseCounter = BLETracker.presenseCandidates.get(bleTag.tag);
                presenseCounter++;

                if (presenseCounter === BLETracker.presenseInRow) {
                    // добавляем в комнату, удаляем из кандидатов, обновляем бд
                    targetHashMap.set(bleTag.tag, 0);
                    BLETracker.presenseCandidates.delete(bleTag.tag);
                    // console.log('tag is presense now!');

                    return {tag: bleTag.tag, absense: false};
                } else {
                    // инкремент счетчика присутствия
                    BLETracker.presenseCandidates.set(bleTag.tag, presenseCounter);
                    // console.log('increment presense counter!');
                    return;
                }
            } else {
                // убираем из кандидатов на присутствие
                BLETracker.presenseCandidates.delete(bleTag.tag);
                // console.log('tag is out of presense candidates!');
                return;
            }
        } else {
            if (bleTag.distance <= BLETracker.limitDistance) {
                // добавляем в кандидаты на присутсвие
                BLETracker.presenseCandidates.set(bleTag.tag, 1);
                // console.log('tag is candidate for presense!');
                return;
            }
        }
    }
}