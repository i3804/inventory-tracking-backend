import * as webpack from 'webpack';
import path from 'path';

const { NODE_ENV } = process.env;
const mode = NODE_ENV === 'production' ? 'production' : 'development';

const config: webpack.Configuration = {
    entry: {
        'auth_server/index': './src/auth_server/index.ts',
        'tag_server/index': './src/tag_server/index.ts',
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js',
        clean: true
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    },
    mode,
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                use: {
                    loader: 'ts-loader'
                },
            }
        ]
    },
    target: 'node',
}

export default config;